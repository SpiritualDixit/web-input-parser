import re


def key_from_str(keyStr):
    import re
    keys = re.findall("\[(\w+)\]+?", keyStr)
    arr = re.findall("^(\w+)", keyStr)
    if len(arr) == 1 and len(keys) >= 1:
        arr = arr[0]
    else:
        arr = False
        keys = []
    return arr, keys


def parse_input_array(arr):
    result = {}

    for arr_key in arr:
        t_var, t_key = key_from_str(arr_key)
        pointer = result
        if t_var:
            if t_var not in pointer:
                pointer[t_var] = {}
            pointer = pointer[t_var]
            temp_pointer = None
            for key in t_key:
                if key not in pointer:
                    pointer[key] = {}
                temp_pointer = pointer
                pointer = pointer[key]

            temp_pointer[key] = arr[arr_key]

        else:
            result[arr_key] = arr[arr_key]

    return result